import { useState } from 'react'

import './App.css'

function App() {
  const [num, setnum] = useState(1)
  function count(){
    setnum(num*5)
  }

  return (
    <>
      <h1>{num}</h1>
      <button onClick={count}>count</button>
    </>
  )
}

export default App
